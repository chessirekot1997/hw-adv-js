'use strict';

const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';

class Card {
	constructor({ id: cardId, title, body }, { name, email }) {
		this.cardId = cardId;
		this.cardTitle = title;
		this.cardText = body;

		this.userName = name;
		this.userEmail = email;

		//Create DOM elements
		this.cardContainer = document.createElement('article');
		this.cardContent = document.createElement('div');
		this.cardHeader = document.createElement('header');
		this.cardBody = document.createElement('div');

		//Buttons
		this.cardHeaderButtons = document.createElement('div');
		this.cardEditButton = document.createElement('button');
		this.cardDeleteButton = document.createElement('button');
	}

	#addClassesToElements() {
		this.cardContainer.classList.add('card');
		this.cardContent.classList.add('card__content');

		//Card header
		this.cardHeader.classList.add('card__header');
		this.cardHeaderButtons.classList.add('card__header-btns');
		this.cardEditButton.classList.add('card__btn', 'card__btn--edit');
		this.cardDeleteButton.classList.add('card__btn', 'card__btn--delete');

		//Card body
		this.cardBody.classList.add('card__body');
	}

	#fillElementsWithContent() {
		this.cardContainer.insertAdjacentHTML('beforeend', `
		<aside class="card__author-avatar">
			<div></div>
		</aside>
	`);

		//Card header
		this.cardHeader.insertAdjacentHTML('beforeend', `
		<div class="card__author">
			<span class="card__author-name">${this.userName}</span>
			<a class="card__author-email" href="mailto:${this.userEmail}">
				${this.userEmail}
			</a>
		</div>
	`);

		this.cardEditButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"><path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"/><path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd"/></svg>`;

		this.cardDeleteButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"/></svg>`;

		this.cardBody.innerHTML = `
			<h3 class="card__title">
				${this.cardTitle}
			</h3>
			<p class="card__text">
				${this.cardText}
			</p>
		`;
	}

	#handleCardEvents() {

		//Delete card
		this.cardDeleteButton.addEventListener('click', () => {
			if (this.cardId > 100) this.cardContainer.remove();
			else {
				fetch(`${postsUrl}/${this.cardId}`, {
					method: "DELETE",
				})
					.then(({ ok, status }) => {
						if (ok === true || status === 404) {
							this.cardContainer.remove();
						}
					})
					.catch(err => {
						console.error(err);
					})
			}
		});

		//Edit button
		this.cardEditButton.addEventListener('click', () => {
			if (this.cardId > 100) {
				this.modalEdit = new ModalEditPost(this, this.cardId, this.cardTitle, this.cardText, true);
			}
			else {
				this.modalEdit = new ModalEditPost(this, this.cardId, this.cardTitle, this.cardText);

				fetch(`${postsUrl}/${this.cardId}`, {
					method: 'PUT',
					body: JSON.stringify({
						title: this.cardTitle,
						body: this.cardText,
					}),
					headers: {
						'Content-Type': 'application/json'
					}
				})
					.then(({ ok }) => {
						if (ok === true) {
							this.modalEdit.isCardEditable = true;
						}
					})
					.catch(err => console.error(err))
			}
			this.modalEdit.render();
		});
	}

	#createElements() {
		this.#addClassesToElements();
		this.#fillElementsWithContent();

		this.cardContainer.append(this.cardContent);
		this.cardContent.append(this.cardHeader, this.cardBody);
		this.cardHeader.append(this.cardHeaderButtons);
		this.cardHeaderButtons.append(this.cardEditButton, this.cardDeleteButton);

		this.#handleCardEvents();
	}

	render(selector) {
		this.#createElements();

		document.querySelector(selector).prepend(this.cardContainer);
	}
}

const drawCards = () => {
	const cardsLoader = document.createElement('div');
	cardsLoader.classList.add('cards__loader');
	cardsLoader.insertAdjacentHTML('beforeend', '<div class="loader"></div>');
	document.querySelector('.cards').append(cardsLoader);

	fetch(postsUrl)
		.then(response => response.json())
		.then(postsData => {

			fetch(usersUrl)
				.then(response => response.json())
				.then(usersData => {
					cardsLoader.remove();

					postsData.forEach((post) => {
						const { userId } = post;

						usersData.forEach(user => {
							const { id } = user;

							if (id === userId) {
								new Card(post, user).render('.cards');
							}
						})
					});
				})
		})
		.catch(err => {
			console.error(err);
		})
}

drawCards();

class Modal {
	constructor() {
		this.modalContainer = document.createElement('div')
		this.modalContent = document.createElement('div');
		this.modalTitle = document.createElement('h2');
		this.modalForm = document.createElement('form');
		this.labelTitle = document.createElement('label');
		this.inputTitle = document.createElement('input');
		this.labelText = document.createElement('label');
		this.inputText = document.createElement('textarea');
		this.buttonsContainer = document.createElement('div');
		this.buttonSubmit = document.createElement('button');
		this.buttonCancel = document.createElement('button');
	}

	_createElements() {
		this.modalContainer.classList.add('modal');
		this.modalContainer.append(this.modalContent);

		this.modalContent.classList.add('modal__content');
		this.modalContent.append(this.modalTitle, this.modalForm);

		this.modalTitle.innerText = 'Модальне вікно';

		this.modalForm.classList.add('modal__form');
		this.modalForm.append(this.labelTitle, this.labelText, this.buttonsContainer);

		this.labelTitle.classList.add('modal__label');
		this.labelTitle.innerText = "Заголовок";
		this.labelTitle.append(this.inputTitle);

		this.inputTitle.classList.add('modal__input');

		this.labelText.classList.add('modal__label');
		this.labelText.innerText = "Текст"
		this.labelText.append(this.inputText);

		this.inputText.classList.add('modal__input');
		this.inputText.rows = '5';

		this.buttonsContainer.classList.add('modal__buttons');
		this.buttonsContainer.append(this.buttonCancel, this.buttonSubmit);

		this.buttonCancel.classList.add('modal__button', 'modal__button--cancel');
		this.buttonCancel.innerText = "Скасувати";

		this.buttonSubmit.classList.add('modal__button', 'modal__button--submit');
		this.buttonSubmit.type = "submit";
		this.buttonSubmit.innerText = "Підтвердити";
	}

	_eventHandlers() {
		this.buttonCancel.addEventListener('click', (e) => {
			e.preventDefault();
			this.modalContainer.remove();
		});
		this.buttonSubmit.addEventListener('click', (e) => {
			e.preventDefault();
		});
	}

	render(selector = 'body') {
		this._createElements();
		this._eventHandlers();

		document.querySelector(selector).append(this.modalContainer);
	}
}

class ModalCreateNewPost extends Modal {
	constructor() {
		super();
	}

	_createElements() {
		super._createElements();

		this.modalTitle.innerText = 'Додати публікацію';
	}

	#createNewPost(title, body) {
		fetch(postsUrl, {
			method: "POST",
			body: JSON.stringify({
				userId: 1,
				title,
				body,
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(postData => {
				const { userId } = postData;

				fetch(`${usersUrl}/${userId}`)
					.then(response => response.json())
					.then((userData) => {
						new Card(postData, userData).render('.cards');
					})
			})
			.catch(err => {
				console.error(err);
			});
	}

	_eventHandlers() {
		super._eventHandlers();
		this.buttonSubmit.addEventListener('click', () => {
			this.#createNewPost(this.inputTitle.value, this.inputText.value);
			this.modalContainer.remove();
		});
	}
}

const createPostSectionHandler = () => {
	const addPostBtn = document.querySelector('.post-add__btn');

	addPostBtn.addEventListener('click', () => {
		new ModalCreateNewPost().render();
	});
}

createPostSectionHandler();

class ModalEditPost extends Modal {
	constructor(cardObj, cardId, title, body, isManuallyCreated = false) {
		super();
		this.cardObj = cardObj;
		this.cardTitle = title;
		this.cardText = body;
		this.isManuallyCreated = isManuallyCreated;
		this.cardId = cardId;

		this.isCardEditable = isManuallyCreated;
		this.editedBody;
	}

	_createElements() {
		super._createElements();

		this.inputTitle.value = this.cardTitle;
		this.inputText.value = this.cardText;

		this.modalTitle.innerText = 'Редагувати публікацію';
	}

	_eventHandlers() {
		super._eventHandlers();
		this.buttonSubmit.addEventListener('click', () => {
			this.editPost();
			this.modalContainer.remove();
		});
	}

	editPost() {
		if (this.isCardEditable === true) {
			this.cardObj['cardTitle'] = this.inputTitle.value;
			this.cardObj['cardText'] = this.inputText.value;

			this.cardObj['cardBody'].innerHTML = `
			<h3 class="card__title">
				${this.inputTitle.value}
			</h3>
			<p class="card__text">
				${this.inputText.value}
			</p>
			`;
		}
	}

	getContent() {
		return this.editPost();
	}
}

