const filmsUrl = 'https://ajax.test-danit.com/api/swapi/films';

const handleFilms = () => {

	const filmsWrapper = document.querySelector('.films__wrapper');

	fetch(filmsUrl)
		.then(response => response.json())
		.then(filmsData => {

			filmsData.forEach(({ episodeId, characters, openingCrawl, name }) => {
				// creating html structure
				const filmContainer = document.createElement('article');
				filmContainer.classList.add('film');

				const charactersContainer = document.createElement('div');
				charactersContainer.classList.add('characters');

				const charactersList = document.createElement('ul');
				charactersList.classList.add('characters__list');

				const charactersLoader = document.createElement('div');
				charactersLoader.classList.add('characters__loader');
				charactersLoader.innerHTML = `<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>`;

				charactersList.append(charactersLoader);

				filmsWrapper.append(filmContainer);

				filmContainer.insertAdjacentHTML('beforeend', `
					<span class="film__episode">Episode ${episodeId}</span>
					<h3 class="film__title">Title: "${name}"</h3>
					<p class="film__introduction">${openingCrawl}</p>
				`);

				filmContainer.append(charactersContainer);
				charactersContainer.insertAdjacentHTML('beforeend', `
					<span class="characters__title">Characters&colon;</span>
				`);
				charactersContainer.append(charactersList);

				characters.forEach(character => {
					fetch(character)
						.then(response => response.json())
						.then(characterData => {
							charactersLoader.remove();
							const { name } = characterData;
							charactersList.insertAdjacentHTML('beforeend', `
								<li class="character">${name}</li>
							`);
						})
						.catch(err => console.log(err));
				});
			});
		})
		.catch(err => console.log(err))
}

handleFilms();