# Відповідь на питання

## Питання

Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

## Відповідь

AJAX - надсилає запити на сервер та отримує з нього дані в фоновому режимі.
Він корисний саме тим, що запити на сервер ніяк не зупиняють роботу сайту та не перезавантажують його, що дає користувачу змогу й далі використовувати сторінку, поки дані надсилаються, чи отримуються.
