"use strict";
const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

class IncorrectDataError extends Error {
	constructor(property) {
		super();
		this.name = 'IncorrectDataError';
		this.message = `${property} is missing`;
	}
}

class Book {
	constructor({ author, name, price }) {
		if (!author) {
			throw new IncorrectDataError('Author');
		}
		if (!name) {
			throw new IncorrectDataError('Name');
		}
		if (!price) {
			throw new IncorrectDataError('Price');
		}

		this.author = author;
		this.name = name;
		this.price = price;
	}

	render(domElement) {

		domElement.insertAdjacentHTML('beforeend', `
		<li>
			<span>Назва: ${this.name}</span>
			<ul>
				<li>Автор: ${this.author}</li>
				<li>Ціна: ${this.price}</li>
			</ul>
		</li>
	`);
	}
}

const showBooks = () => {
	const ul = document.createElement('ul');

	document.querySelector('#root').append(ul);

	books.forEach(book => {
		try {
			new Book(book).render(ul);
		}
		catch (err) {
			if (err instanceof IncorrectDataError) {
				console.warn(err);
			}
			else {
				throw err;
			}
		}
	});
}

showBooks();