# Відповідь на питання

Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.

- Коли потрібно перехватити помилки або опрацювати власні виключення.
- Коли вікористовуються дані від третьої особи;
- Коли дані надходять з серверу;
