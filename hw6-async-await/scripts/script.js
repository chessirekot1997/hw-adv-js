const getIP = async () => {
	try {
		const ipResponse = await fetch('https://api.ipify.org/?format=json').then(response => response.json());
		return ipResponse.ip;
	}
	catch (err) {
		console.error(err);
	}
}

const drawIP = async () => {
	const ipEl = document.querySelector('.find-output__ip-address');
	ipEl.innerText = await getIP();
}

const getInformationGotByIP = async () => {
	const IP = await getIP();

	try {
		const IpFieldsData = await fetch(`http://ip-api.com/json/${IP}?fields=status,continent,country,regionName,city,district`).then(response => response.json());

		if (IpFieldsData.status === "success") {
			return IpFieldsData;
		}
	}
	catch (err) {
		console.error(err);
	}
}

const findbyIpButton = document.querySelector('.find__btn');

const showInformationGotByIP = async () => {
	const outputTableBody = document.querySelector('.find-output__table-body');
	const { continent, country, regionName, city, district } = await getInformationGotByIP();

	outputTableBody.innerHTML = `
		<tbody>
			<tr>
				<th>Континент</th>
				<td>${continent || '-'}</td>
			</tr>
			<tr>
				<th>Країна</th>
				<td>${country || '-'}</td>
			</tr>
			<tr>
				<th>Регіон</th>
				<td>${regionName || '-'}</td>
			</tr>
			<tr>
				<th>Місто</th>
				<td>${city || '-'}</td>
			</tr>
			<tr>
				<th>Район</th>
				<td>${district || '-'}</td>
			</tr>
		</tbody>
	`;
}

drawIP();

findbyIpButton.addEventListener('click', showInformationGotByIP);
