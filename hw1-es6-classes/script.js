class Employee {
	constructor(name, age, salary) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}

	get name() {
		return this._name;
	}

	get age() {
		return this._age;
	}

	get salary() {
		return this._salary;
	}

	set name(value) {
		this._name = value;
	}

	set age(value) {
		this._age = value;
	}

	set salary(value) {
		this._salary = value;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this._lang = lang;
	}

	get salary() {
		return super.salary * 3;
	}
}

console.log(new Programmer('Victor', 20, 2000, 'Python').salary);
console.log(new Programmer('Fedir', 32, 3200, 'C#').salary);
console.log(new Programmer('Mike', 23, 2300, 'Java').salary);
console.log(new Programmer('Richard', 25, 2500, 'JS').salary);